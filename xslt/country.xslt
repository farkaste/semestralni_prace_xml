<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <xsl:template match="/">
        <html>
            <head>
                <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css"
                      rel="stylesheet"
                      integrity="sha384-giJF6kkoqNQ00vy+HMDP7azOuL0xtbfIcaT9wjKHr8RbDVddVHyTfAAsrekwKmP1"
                      crossorigin="anonymous"/>
            </head>
            <body>
                <div class="container">
                    <div class="pt-10">
                        <div class="row pt-5">
                            <div class="col-md-6" style="margin-top: auto; margin-bottom: auto;">
                                    <h1 class="text-center py-5" style="font-size: 50px">
                                        <xsl:value-of select="country/introduction/name"/>
                                    </h1>
                            </div>
                            <div class="col-md-6">
                                <xsl:element name="img">
                                    <xsl:attribute name="src">
                                        <xsl:value-of select="country/introduction/image/@source"/>
                                    </xsl:attribute>
                                    <xsl:attribute name="style">
                                        <xsl:text>
                                            width: 400px;
                                            height: auto;
                                            border-radius: 20px;
                                        </xsl:text>
                                    </xsl:attribute>
                                </xsl:element>
                            </div>
                        </div>
                        <xsl:apply-templates select="country/navigation"/>
                        <xsl:apply-templates select="country/introduction"/>
                    </div>
                    <div class="w-75">
                        <xsl:apply-templates select="country/group"/>
                    </div>
                </div>
            </body>
        </html>

    </xsl:template>

    <xsl:template match="navigation">
        <div class="row py-5">
            <h2 class="pb-5">Navigation</h2>
            <xsl:apply-templates/>
        </div>
    </xsl:template>

    <xsl:template match="navigation_item">
        <div class="col-md-2 col-sm-12 py-3">
            <xsl:element name="a">
                <xsl:attribute name="href">
                    <xsl:value-of select="@href"/>
                </xsl:attribute>
                <xsl:attribute name="style">
                    text-decoration: none;
                    border: 1px solid;
                    padding: 13px;
                    border-radius: 17px;
                    font-size: 15px;
                </xsl:attribute>
                <xsl:value-of select="."/>
            </xsl:element>
        </div>
    </xsl:template>

    <xsl:template match="introduction">
        <h2 id="Introduction">Introduction
        <h3>Background</h3>
        <p>
            <xsl:value-of select="background"/>
        </p>
        </h2>
    </xsl:template>

    <xsl:template match="group">
        <xsl:element name="h2">
            <xsl:attribute name="style">font-weight: 600</xsl:attribute>
            <xsl:attribute name="id"><xsl:value-of select="@name"/></xsl:attribute>
            <xsl:value-of select="@name"/>
        </xsl:element>
        <xsl:apply-templates select="section"/>
    </xsl:template>

    <xsl:template match="section">
        <h3 style="font-weight: 600">
            <xsl:value-of select="@title"/>
        </h3>
        <ul style="list-style-type: none;">
            <xsl:apply-templates/>
        </ul>
    </xsl:template>

    <xsl:template match="detail-list">
        <xsl:apply-templates/>
    </xsl:template>

    <xsl:template match="detail[@url]">
        <xsl:element name="a">
            <xsl:attribute name="href">
                <xsl:value-of select="@url"/>
            </xsl:attribute>
            <xsl:attribute name="target">
                _blank
            </xsl:attribute>
            <xsl:attribute name="style">text-decoration: none</xsl:attribute>
            <xsl:value-of select="."/>
        </xsl:element>
    </xsl:template>
    <xsl:template match="detail[@name]">
        <li>
            <strong>
                <xsl:value-of select="@name"/>:
            </strong>
            <em>
                <xsl:value-of select="@value"/>
            </em>
            <xsl:text> </xsl:text>
            <xsl:value-of select="@unit"/>
            <xsl:text> </xsl:text>
            <xsl:value-of select="."/>
            <ul>
                <xsl:apply-templates select="detail_inner"/>
            </ul>
        </li>
    </xsl:template>

    <xsl:template match="detail">
        <li>
            <em>
                <xsl:value-of select="@value"/>
            </em>
            <xsl:text> </xsl:text>
            <xsl:value-of select="@unit"/>
            <xsl:text> </xsl:text>
            <xsl:value-of select="."/>
            <ul>
                <xsl:apply-templates select="detail_inner"/>
            </ul>
        </li>
    </xsl:template>

     <xsl:template match="detail_inner">
        <li>
            <strong>
                <xsl:value-of select="@name"/>:
            </strong>
            <em>
                <xsl:value-of select="@value"/>
            </em>
            <xsl:text> </xsl:text>
            <xsl:value-of select="@unit"/>
            <xsl:text> </xsl:text>
            <xsl:value-of select="."/>
        </li>
    </xsl:template>

</xsl:stylesheet>


