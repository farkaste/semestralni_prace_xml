<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <xsl:template match="/countries">
        <html>
            <head>
                <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css"
                      rel="stylesheet"
                      integrity="sha384-giJF6kkoqNQ00vy+HMDP7azOuL0xtbfIcaT9wjKHr8RbDVddVHyTfAAsrekwKmP1"
                      crossorigin="anonymous"/>
            </head>
            <body>
                <div class="container py-5">
                    <div class="pb-5">
                        <h2>Semestral work</h2>
                        <span>Subject: BI-XML</span>
                        <br/>
                        <span>Author: Štěpán Farka</span>
                        <br/>
                        <span>fit-username: farkaste</span>
                        <br/>
                    </div>

                    <h1 class="pb-5">Countries</h1>
                    <div class="row">
                        <xsl:apply-templates select="country"/>
                    </div>
                </div>
            </body>
        </html>

    </xsl:template>

    <xsl:template match="country">
        <div class="col-md-3">
            <div class="container">
                <div class="card" style="width: 18rem;">
                    <xsl:element name="img">
                        <xsl:attribute name="src">
                            <xsl:value-of select="introduction/image/@source"/>
                        </xsl:attribute>
                        <xsl:attribute name="class">
                            <xsl:text>card-img-top</xsl:text>
                        </xsl:attribute>
                        <xsl:attribute name="style">
                            <xsl:text>height: 200px;</xsl:text>
                        </xsl:attribute>
                    </xsl:element>
                    <div class="card-body">
                        <h5 class="card-title"><xsl:value-of select="introduction/name"/></h5>
                         <xsl:element name="a">
                            <xsl:attribute name="href">
                                <xsl:value-of select="@project_html"/>
                            </xsl:attribute>
                            <xsl:attribute name="class">
                                <xsl:text>btn btn-primary</xsl:text>
                            </xsl:attribute>
                             <xsl:attribute name="style">
                                 <xsl:text>margin-right: 10px;</xsl:text>
                             </xsl:attribute>
                            Project page
                        </xsl:element>
                         <xsl:element name="a">
                            <xsl:attribute name="href">
                                <xsl:value-of select="@url"/>
                            </xsl:attribute>
                            <xsl:attribute name="class">
                                <xsl:text>btn btn-primary</xsl:text>
                            </xsl:attribute>
                            Original page
                        </xsl:element>
                    </div>
                </div>
            </div>
        </div>
    </xsl:template>
</xsl:stylesheet>