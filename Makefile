countries = xml_files/australia.xml xml_files/albania.xml xml_files/czechia.xml  xml_files/ukraine.xml

final: dtd_validation rnc_validation create_single_xml create_html_for_all_countries create_index_html
	$(info ********* SUCCESS *********)

dtd_validation: $(countries)
	xmllint --noout --dtdvalid dtd_files/country.dtd $(countries)
rnc_validation: $(countries)
	java -jar jar_files/jing.jar -c -c rnc_files/country.rnc $(countries)
create_single_xml: $(countries)
	xmllint --noent --dropdtd dtd_files/countries.dtd  > xml_files/all_countries.xml

create_html_for_all_countries: create_australia_html create_albania_html create_czechia_html create_ukraine_html

create_australia_html:
	java -jar jar_files/saxon-he-10.3.jar -xsl:xslt/country.xslt -s:xml_files/australia.xml > generated_html/australia.html
create_albania_html:
	java -jar jar_files/saxon-he-10.3.jar -xsl:xslt/country.xslt -s:xml_files/albania.xml > generated_html/albania.html
create_czechia_html:
	java -jar jar_files/saxon-he-10.3.jar -xsl:xslt/country.xslt -s:xml_files/czechia.xml > generated_html/czechia.html
create_ukraine_html:
	java -jar jar_files/saxon-he-10.3.jar -xsl:xslt/country.xslt -s:xml_files/ukraine.xml > generated_html/ukraine.html


create_index_html:
	java -jar jar_files/saxon-he-10.3.jar -xsl:xslt/index.xslt -s:xml_files/all_countries.xml > generated_html/index.html